import type { PatientsRegister } from "hospital-lib";

export const patientRegisterToList = (patients: PatientsRegister) => {
  return Object.keys(patients).map(
    (patient) => `${patient}: ${patients[patient]}`
  );
};
