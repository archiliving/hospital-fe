import type { Drug, PatientsRegister } from "hospital-lib";

export interface Log {
  drugs: Drug[];
  input: PatientsRegister;
  output?: PatientsRegister;
}
