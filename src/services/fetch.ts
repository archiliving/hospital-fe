const BASE_URL = "http://localhost:7200";

export default {
  async getApi(url: string) {
    let error: { code?: number; message: string } | null = null;
    let data: string[] = [];

    try {
      const response = await fetch(`${BASE_URL}/${url}`);
      if (response.ok) {
        const finalRes = await response.text();
        data = finalRes.replace(/"/g, "").toUpperCase().split(",");
      } else {
        error = { code: response.status, message: response.statusText };
      }
    } catch {
      error = { message: "Failed to fetch" };
    }

    return { data, error };
  },
};
