import fetch from "./fetch";

export default {
  getDrugs() {
    return fetch.getApi("drugs");
  },
};
