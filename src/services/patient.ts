import fetch from "./fetch";

export default {
  getPatientsHealthStatus() {
    return fetch.getApi("patients");
  },
};
